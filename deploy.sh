#!/bin/bash
export NEAR_ENV=testnet
export ACCOUNT_ID=nhtera.testnet
export CONTRACT_ID=price-oracle-dashboard.testnet

################## B1: Deploy contract ##################
echo "###################### Build Contract #####################"
./build.sh

echo "################### DEPLOY CONTRACT ###################"
near deploy $CONTRACT_ID --accountId $ACCOUNT_ID --wasmFile ./res/price_oracle_dashboard.wasm

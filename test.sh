#!/bin/bash
export NEAR_ENV=testnet
export ACCOUNT_ID=nhtera.testnet
export CONTRACT_ID=price-oracle.nearlend-official.testnet

# Get price data from price oracle contract
near view $CONTRACT_ID get_price_data


# get price data from price oracle contract by account id
near view $CONTRACT_ID get_oracle_price_data '{"account_id": "bot-ft-1.nearlend-official.testnet", "asset_ids": ["wrap.testnet", "usdt.fakes.testnet"]}'
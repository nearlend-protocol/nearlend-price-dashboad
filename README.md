# priceoracle-data-dashboard

NEAR Native Price Oracle on-chain dashboard

Live-demo:
https://price-oracle-dashboard.testnet.page/

## Techstack:

[Web4](https://github.com/vgrichina/web4) is a new way to distribute decentralized apps. You only need to deploy one smart contract using WebAssembly to deploy whole web app:
https://web4.near.page/
